INSTALL
============
1. Install nodejs, Install and setup MongoDB
2. Goto to project folder and run `npm install`
3. Edit `config/db.js`
4. Run `node server.js`
5. Open `http://localhost:2181/`