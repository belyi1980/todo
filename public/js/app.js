/**
 * Created by EugeneB on 29/01/2016.
 */

angular.module('todoApp',['ngRoute','appRoutes','MainCtrl', 'TodoService']);

angular.module('appRoutes',[]).config(['$routeProvider', '$locationProvider',function($routeProvider, $locationProvider){

    $routeProvider
        .when('/', {
            templateUrl: 'views/default.html',
            controller : 'MainController'
        })
        .otherwise({
            templateUrl: 'views/default.html',
            controller : 'MainController'
        });

    $locationProvider.html5Mode(true);

}]);