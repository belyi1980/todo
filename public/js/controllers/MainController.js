/**
 * Created by EugeneB on 29/01/2016.
 */

angular.module('MainCtrl',[]).controller('MainController', ['$scope', '$http', 'Todo', function($scope, $http, Todo){

    $scope.getAll = function() {
        Todo.get().success(function(data){
            $scope.todos = data;
        });

    };

    $scope.createNewTask = function() {
        console.log('submit !');
        if (!$.isEmptyObject($scope.formData)) {
            Todo.create($scope.formData).success(function (data) {
                $scope.formData = {};
                $scope.getAll();
            });
        }
    };

    $scope.removeTask = function(id) {
        if (String(id).length > 0) {
            Todo.delete(id).success(function(data){
                $scope.getAll();
            });
        }
    };

    $scope.completeTask = function(id) {
        if (String(id).length > 0) {
            Todo.update(id).success(function(data){
                $scope.getAll();
            });
        }
    };

    //Get all tasks
    $scope.getAll();


}]);