/**
 * Created by EugeneB on 29/01/2016.
 */

angular.module('TodoService', []).factory('Todo', ['$http', function($http){
    return {
        get: function() {
            return $http.get('api/tasks');
        },

        create: function(data) {
            return $http.post('api/task', data);
        },
        delete : function(id) {
            return $http.delete('/api/task/' + id);
        },
        update: function(id) {
            return $http.put('/api/task/' + id);
        }
    }
}]);