/**
 * Created by EugeneB on 29/01/2016.
 */

var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var todoSchema = new Schema({
        taskTitle: {type: String, required: true},
        completed: {type: Boolean, default: false},
        dateCreated: { type: Date, default: Date.now},
        dateCompleted: { type: Date, default: null}
    });

//
//todoSchema.pre('save', function(next){
//   if (this.isNew) {
//       next();
//   }
//});


var ToDo = mongoose.model('ToDo', todoSchema);

module.exports = ToDo;