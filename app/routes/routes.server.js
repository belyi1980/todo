/**
 * Created by EugeneB on 29/01/2016.
 */

var ToDo = require('../models/model.server.todo');

module.exports = function(app) {


    app.get('/api/tasks', function(req, res) {
        ToDo.find({}, function(err, tasks) {
            if (err) throw err;

            // object of all the tasks
            res.json(tasks)

        });
    });

    app.post('/api/task', function(req, res){
        var task = new ToDo({taskTitle: req.body.taskTitle});

        task.save(function(err, task){
            if (err) {
                res.send(err);
            } else if(task._id) {
                res.json({error: 0});
            }
        });
    }) ;

    app.delete('/api/task/:_id', function (req, res) {
        ToDo.remove({
            _id: req.params._id
        }, function (err, task) {
            if (err)
                res.send(err);
            else
                res.json({error: 0});
        });
    });

    app.put('/api/task/:_id', function(req, res) {
       //consiole.log('BODY' , req.)
        ToDo.findOne({_id: req.params._id}, function(err, task){
            if (err)
                res.send(err);
            else {
                task.completed = true;
                task.dateCompleted = new Date();
                task.save(function(err, task){
                    if (err)
                        res.send(err);
                    else
                        res.json({error: 0});
                });
            }
        });
    });

    app.get('*', function(req, res){
       res.sendfile('./public/index.html');
    });

}
