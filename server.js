/**
 * Created by EugeneB on 29/01/2016.
 */

// MODULES
var express         = require('express');
var app             = express();
var bodyParser      = require('body-parser');
var morgan          = require('morgan');
var methodOverride  = require('method-override');
var mongoose        = require('mongoose');

// CONFIG
var db              = require('./config/db');
var port            = 2181;

mongoose.connect(db.url);

//INIT

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.json({type: 'application/vnd.api+json'}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(express.static(__dirname + '/public'));

// ROUTE
require('./app/routes/routes.server')(app);

//START
app.listen(port);

console.log('App started [OK]');

exports = module.exports = app;